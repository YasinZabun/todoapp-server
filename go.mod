module TodoApp-Server

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/golang/mock v1.6.0
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/segmentio/pointer v0.0.0-20210208225806-37b5111c9ced // indirect
	github.com/stretchr/testify v1.4.0
	github.com/valyala/fasttemplate v1.2.1 // indirect
	github.com/zemirco/couchdb v0.0.0-20170316052722-83ed906ea1f0
	github.com/zemirco/uid v0.0.0-20160129141151-3763f3c45832 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect

)

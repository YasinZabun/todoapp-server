package main

import (
	"TodoApp-Server/handler"
	"TodoApp-Server/repository"
	"TodoApp-Server/service"
	"fmt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"os"

	configuration "TodoApp-Server/config"
)

func main() {
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))

	//e.Use(middleware.CORS())
	todoHandler := getService()
	println("name = ", os.Getenv("COUCHDB_NAME"))
	println("url = ", os.Getenv("COUCHDB_URL"))
	println("password = ", os.Getenv("COUCHDB_PASSWORD"))
	println("user = ", os.Getenv("COUCHDB_USER"))
	e.GET("/todo/alltodo", todoHandler.GetTodos)
	e.POST("/todo/addtodo", todoHandler.CreateTodo)
	e.Logger.Fatal(e.Start(":5858"))

}
func getService() handler.Handler {

	collection := new(configuration.CouchDBConfiguration).Init()
	if collection.Database == nil {
		fmt.Println("database cannot be nil")
		os.Exit(-1)
	}

	repository := repository.NewTodoRepository(collection.Database)
	todoService := service.NewTodoService(repository)
	todoHandler := handler.NewTodoHandler(todoService)
	return todoHandler
}

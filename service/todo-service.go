//go:generate mockgen -source todo-service.go -destination mock-todo-service.go -package service
package service

import (
	"TodoApp-Server/model"
	errorHandler "TodoApp-Server/model/common/error"
	"TodoApp-Server/repository"
)

type Service interface {
	GetTodos() interface{}
	CreateTodo(i interface{}) interface{}
}

type TodoService struct {
	repository repository.Repository
}

func NewTodoService(repository repository.Repository) Service {
	return TodoService{repository}
}

func (t TodoService) GetTodos() interface{} {
	return t.repository.GetTodos()
}

func (t TodoService) CreateTodo(i interface{}) interface{} {
	req, err := i.(model.TodoModel)
	if !err {
		return errorHandler.BadRequestError{Message: "Request is invalid!"}
	}
	if req.Content == "" {
		return errorHandler.BadRequestError{Message: "Todo content cannot be empty!"}
	}
	return t.repository.CreateTodo(req)
}

package service

import (
	"TodoApp-Server/model"
	"TodoApp-Server/repository"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestGetTodos(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	var todoList []model.TodoModel
	todoList = append(todoList, model.TodoModel{
		Content:   "buy some milk",
		Completed: false,
	})
	repository := repository.NewMockRepository(controller)
	repository.EXPECT().GetTodos().Return(todoList).Times(1)
	service := TodoService{repository}

	all := service.GetTodos().([]model.TodoModel)

	assert.NotNil(t, all)
	assert.NotEmpty(t, all)
	assert.Equal(t, 1, len(all))
	assert.Equal(t, "buy some milk", all[0].Content)
	//assert.False(t, all[0].Content)
}

func TestCreateOne(t *testing.T) {

	controller := gomock.NewController(t)
	defer controller.Finish()

	repository := repository.NewMockRepository(controller)
	repository.EXPECT().CreateTodo(model.TodoModel{Content: "buy some milk", Completed: false}).Return(model.TodoModel{
		Content:   "buy some milk",
		Completed: false,
	}).Times(1)

	service := NewTodoService(repository)
	created := service.CreateTodo(model.TodoModel{Content: "buy some milk", Completed: false})

	assert.NotNil(t, created)
	todo := created.(model.TodoModel)
	assert.Equal(t, "buy some milk", todo.Content)
	assert.False(t, todo.Completed)
}

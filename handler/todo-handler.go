package handler

import (
	"TodoApp-Server/model"
	errorHandler "TodoApp-Server/model/common/error"
	"TodoApp-Server/service"
	"github.com/labstack/echo"
	"net/http"
)

type TodoHandler struct {
	service service.Service
}

type Handler interface {
	GetTodos(c echo.Context) error
	CreateTodo(c echo.Context) error
}

func NewTodoHandler(service service.Service) Handler {
	return TodoHandler{service}
}
func (s TodoHandler) GetTodos(c echo.Context) error {
	todolist := s.service.GetTodos()
	if todolist == nil {
		return c.JSON(http.StatusNoContent, nil)
	}
	return c.JSON(http.StatusOK, todolist)
}

func (s TodoHandler) CreateTodo(c echo.Context) error {
	u := &model.TodoModel{}
	if err := c.Bind(u); err != nil {
		return err
	}
	reqModel := model.TodoModel{Content: u.Content}
	res := s.service.CreateTodo(reqModel)
	err := extractError(res)
	if err != nil {
		return c.JSON(err.GetStatus(), res)
	}
	return c.JSON(http.StatusCreated, res)
}
func extractError(i interface{}) errorHandler.Error {
	error, ok := i.(errorHandler.Error)
	if ok {
		return error
	}
	return nil
}

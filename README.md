## Prerequisites

* Golang 1.16.x
* Docker 20.10+
* Docker Compose 1.25+

## Installation

```sh
docker-compose up -d 
```

# Running test

```
$ go test ./... 
```

# Create mocks

```
$ go generate -v ./... 
```

## Used Technologies

* Golang 1.16.7
* Echo
* CouchDB

## This project will serve at [this](http://52.156.77.163:8080/) address for a certain period of time



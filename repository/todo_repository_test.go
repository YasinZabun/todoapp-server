package repository

import (
	configuration "TodoApp-Server/config"
	"TodoApp-Server/model"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/zemirco/couchdb"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
)

var couchRepo = new(configuration.CouchDBConfiguration).Init()
var goRepo = NewTodoRepository(couchRepo.Database)

func TestShouldAddTodoToDb(t *testing.T) {
	todoReturnedTestModel := goRepo.CreateTodo(model.TodoModel{Completed: false, Content: "repo test todo"})
	todoConvertedReturnedTestModel := todoReturnedTestModel.(*model.TodoModel)

	defer delTodo(todoConvertedReturnedTestModel.ID, todoConvertedReturnedTestModel.Rev)

	docs, err := getAllTodo()
	if assert.NoError(t, err) {
		assert.Condition(t, func() (success bool) {
			for i := 0; i < len(docs.Rows); i++ {
				if docs.Rows[i].ID == todoConvertedReturnedTestModel.ID {
					return true
				}
			}
			return false
		}, docs)
	}
}
func TestShouldReturnAllTodoFromDb(t *testing.T) {
	todoReturnedTestModel, _ := addTodo(false)
	defer delTodo(todoReturnedTestModel.ID, todoReturnedTestModel.Rev)
	docs := goRepo.GetTodos()
	testTodoModels, ok := docs.([]*model.TodoModel)
	if ok {
		assert.Condition(t, func() (success bool) {
			for i := 0; i < len(testTodoModels); i++ {
				if testTodoModels[i].ID == todoReturnedTestModel.ID {
					return true
				}
			}
			return false
		}, docs)
	}

}
func addTodo(complete bool) (*couchdb.DocumentResponse, error) {
	todoTestModel := model.TodoModel{Content: "test repo todo", Completed: complete}
	return couchRepo.Database.Post(&todoTestModel)
}
func delTodo(id string, rev string) {
	couchRepo.Database.Delete(&couchdb.Document{ID: id, Rev: rev})
}
func getAllTodo() (*couchdb.ViewResponse, error) {
	return couchRepo.Database.AllDocs(nil)
}
func getTodoWithGivenId(id string) (*model.TodoModel, error) {
	client := &http.Client{}
	url := os.Getenv("COUCHDB_URL") + "tododb/" + id
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	req.Header.Add("Authorization", "Basic cm9vdDpyb290")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	var item model.TodoModel
	json.Unmarshal(body, &item)
	return &item, nil
}

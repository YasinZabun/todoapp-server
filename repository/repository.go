//go:generate mockgen -source repository.go -destination mock-repository.go -package repository
package repository

import (
	"TodoApp-Server/model"
	"encoding/json"
	"fmt"
	"github.com/zemirco/couchdb"
	"io/ioutil"
	"net/http"
	"os"
)

type Repository interface {
	GetTodos() interface{}
	CreateTodo(i interface{}) interface{}
}
type TodoRepository struct {
	Db couchdb.DatabaseService
}

func NewTodoRepository(db couchdb.DatabaseService) Repository {
	return TodoRepository{Db: db}
}

func (t TodoRepository) GetTodos() interface{} {
	var results []*model.TodoModel
	object, _ := t.Db.AllDocs(nil)
	for _, v := range object.Rows {

		client := &http.Client{}
		url := os.Getenv("COUCHDB_URL") + "tododb/" + v.ID
		req, err := http.NewRequest("GET", url, nil)

		if err != nil {
			fmt.Println(err)
			return err
		}
		req.Header.Add("Authorization", "Basic cm9vdDpyb290")

		res, err := client.Do(req)
		if err != nil {
			fmt.Println(err)
			return err
		}
		defer res.Body.Close()

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			fmt.Println(err)
			return err
		}
		var item model.TodoModel
		json.Unmarshal(body, &item)
		results = append(results, &item)

	}
	return results
}

func (t TodoRepository) CreateTodo(i interface{}) interface{} {
	todoReq, _ := i.(model.TodoModel)
	doc := &model.TodoModel{Content: todoReq.Content, Completed: todoReq.Completed}
	res, err := t.Db.Post(doc)
	if err != nil {
		fmt.Println(err)
		return err
	}
	doc.Rev = res.Rev
	doc.ID = res.ID
	return doc
}
